/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file cat.h
/// @version 1.0
///
/// @author Jared Inouye <jinouye7@hawaii.edu>
/// @date 10_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//

#pragma once

const double CAT_POWER_IN_A_JOULE = 0;

const char CAT_POWER = 'c';

double fromCatPowerToJoule( double catPower );

double fromJouletoCatPower( double joule);


