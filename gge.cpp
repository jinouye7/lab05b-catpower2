///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.cpp
/// @version 1.0
///
/// @author Jared Inouye <jinouye7@hawaii.edu>
/// @date 10_Feb_2022
///////////////////////////////////////////////////////////////////////////////



#include "gge.h"

double fromGasToJoule( double gas ) {
   return gas / GASOLINE_GALLONS_EQUIVALENT_IN_A_JOULE ;
}


double fromJouleToGas( double joule ) {
   return joule * GASOLINE_GALLONS_EQUIVALENT_IN_A_JOULE ;
}

